function A3B_createMeanBOLD_ER_nii(ID)

% creae Niftys from cleaned data matrices

mode = 'local';
%mode = 'tardis';

if strcmp(mode, 'tardis')
    pn.root         = '/home/mpib/LNDG/StateSwitch/WIP/';
    pn.standards    = [pn.root, 'B6_PLS_eventRelated/B_data/A_standards/'];
    pn.timing       = [pn.root, 'B6_PLS_eventRelated/B_data/A_regressors/']; % location of info regarding run timing matrices
    pn.coordpath    = [pn.root, 'B6_PLS_eventRelated/B_data/VoxelOverlap/'];
    pn.PLSfilesIn	= [pn.root, 'B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/'];
    pn.PLSfilesOut	= [pn.root, 'B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/'];
    pn.cleanDataOut	= [pn.root, 'B6_PLS_eventRelated/B_data/cleanedData/'];
    pn.preprocFiles = [pn.root, 'preproc/B_data/'];
    pn.out = '/home/beegfs/kosciessa/StateSwitch/WIP/G_GLM/B_data/BOLDin/';
    % The following toolboxes need to be added using the prepare script:
    % rs-fMRI-master
    % NIFTI_toolbox
    % preprocessing_tools
else
    restoredefaultpath;
    %pn.root         = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/';
    pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
    pn.tools        = [pn.root, 'analyses/B_extendedPreproc/T_tools/'];  addpath(genpath(pn.tools));
    pn.coordpath    = [pn.root, 'analyses/B4_PLS_preproc2/B_data/VoxelOverlap/'];
    pn.PLSfilesIn	= [pn.root, 'analyses/B_extendedPreproc/B_data/MeanBOLD_ER_PLS_v4/'];
    pn.PLSfilesOut	= [pn.root, 'analyses/B_extendedPreproc/B_data/MeanBOLD_ER_PLS_v4/'];
    pn.cleanDataOut	= [pn.root, 'analyses/B_extendedPreproc/B_data/cleanedData/'];
    pn.standards    = [pn.root, 'analyses/B_extendedPreproc/B_data/A_standards/'];
	pn.timing       = [pn.root, 'analyses/B_extendedPreproc/B_data/A_regressors/']; % location of info regarding run timing matrices
    pn.out = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/G_GLM/B_data/BOLDin/';
    % add Powers et al. scrubbing toolbox
    addpath(genpath([pn.root, 'analyses/B_extendedPreproc/T_tools/rs-fMRI-master']))
end

    disp(['Processing subject ', ID, '.']);

    % load datamat with necessary info
    a = load([pn.PLSfilesIn, 'meanBOLD_ER_', ID, '_fMRIsessiondata.mat']);
    
    % load common coordinates
    load([pn.coordpath, 'coords_N95.mat'], 'final_coords');
    
    %% Load NIfTI file

    for indRun = 1:4

        %% Load NIfTI
        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        % if proc on tardis: change path to tardis preproc directory
       
        if strcmp(mode, 'tardis')
            if strcmp(ID, '1126') % for 1126 choose nlreg without intermediate t2 step as no t2 was recorded
                fileName = [ID, '_run-',num2str(indRun),'_feat_detrended_highpassed_denoised_nlreg_2009c_3mm.nii'];
            else
                fileName = [ID, '_run-',num2str(indRun),'_feat_detrended_highpassed_denoised_t2nlreg_2009c_3mm.nii'];
            end
            fname = [pn.preprocFiles,  'D_preproc/',ID, '/preproc2/run-',num2str(indRun), '/' fileName];
        else
            fname = [a.session_info.run(indRun).data_path '/' a.session_info.run(indRun).data_files{:}];
        end
        
        if ~exist(fname) || isempty(a.session_info.run(indRun).data_files)
            warning(['File not available: ', ID, ' Run ', num2str(indRun)]);
            continue;
        end
        
        [img] = load_nii(fname);
        
        %% load cleaned data
        
        load([pn.cleanDataOut, ID, '_run-',num2str(indRun), '_postProcessed.mat'], 'data_clean')        
        
        %% replace data in structure
        
        data_complete = zeros(size(img.img,1)*size(img.img,2)*size(img.img,3),size(data_clean,2));
        data_complete(final_coords,:) = data_clean;
        
        img.img = reshape(data_complete, size(img.img));
       
        %% save as nifty
        
        save_nii(img, [pn.out, ID, '_run-',num2str(indRun), '_regressed.nii']);
        
    end