function A3_createMeanBOLD_ER(ID)

% - create meanBOLD data matrices for PLS
% - regress out 6 DOF motion parameters, CSF and WM mean signals
% - censor significant DVARS volumes by spectral interpolation (Powers et al., 2014)

%mode = 'local';
mode = 'tardis';

if strcmp(mode, 'tardis')
    pn.root         = '/home/mpib/LNDG/StateSwitch/WIP/';
    pn.standards    = [pn.root, 'B6_PLS_eventRelated/B_data/A_standards/'];
    pn.timing       = [pn.root, 'B6_PLS_eventRelated/B_data/A_regressors/']; % location of info regarding run timing matrices
    pn.coordpath    = [pn.root, 'B6_PLS_eventRelated/B_data/VoxelOverlap/'];
    pn.PLSfilesIn	= [pn.root, 'B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/'];
    pn.PLSfilesOut	= [pn.root, 'B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/'];
    pn.cleanDataOut	= [pn.root, 'B6_PLS_eventRelated/B_data/cleanedData/'];
    pn.preprocFiles = [pn.root, 'preproc/B_data/'];
    % The following toolboxes need to be added using the prepare script:
    % rs-fMRI-master
    % NIFTI_toolbox
    % preprocessing_tools
else
    restoredefaultpath;
    pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
    pn.tools        = [pn.root, 'analyses/B_extendedPreproc/T_tools/'];  addpath(genpath(pn.tools));
    pn.coordpath    = [pn.root, 'analyses/B4_PLS_preproc2/B_data/VoxelOverlap/'];
    pn.PLSfilesIn	= [pn.root, 'analyses/B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/'];
    pn.PLSfilesOut	= [pn.root, 'analyses/B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4/'];
    pn.cleanDataOut	= [pn.root, 'analyses/B6_PLS_eventRelated/B_data/cleanedData/'];
    pn.standards    = [pn.root, 'analyses/B6_PLS_eventRelated/B_data/A_standards/'];
	pn.timing       = [pn.root, 'analyses/B6_PLS_eventRelated/B_data/A_regressors/']; % location of info regarding run timing matrices
    pn.cleanDataOut	= [pn.root, 'analyses/B_extendedPreproc/B_data/cleanedData/'];
    % add Powers et al. scrubbing toolbox
    addpath(genpath([pn.root, 'analyses/B4_PLS_preproc2/T_tools/rs-fMRI-master']))
end

    numConds_raw = 4; % number of conditions may have changed in PLS input structure during preproc

    disp(['Processing subject ', ID, '.']);

%% create the SDBOLD datamats

    % load subject's sessiondata file
    a = load([pn.PLSfilesIn, 'meanBOLD_ER_', ID, '_fMRIsessiondata.mat']);

    % load common coordinates
    load([pn.coordpath, 'coords_N95.mat'], 'final_coords');
    
    conditions=a.session_info.condition(1:numConds_raw);
    a = rmfield(a,'st_datamat'); %4*1220060
    a = rmfield(a,'st_coords');

    %replace fields with correct info.
    a.session_info.datamat_prefix   = (['meanBOLD_ER_', ID, '_v4']); % SD PLS file name; _Bfmrisessiondata will be automatically appended!
    a.st_coords = final_coords;     % constrain analysis to shared non-zero GM voxels
    a.pls_data_path = pn.PLSfilesOut;

    %% Load NIfTI file

    blockCount(1:4) = 0;
    block_data_tmp = NaN(4,16*4,size(final_coords,1),17); % preallocate matrix with NaNs to avoid zero-encodings
    for indRun = 1:a.session_info.num_runs

        %% Load NIfTI
        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        % if proc on tardis: change path to tardis preproc directory
       
        if strcmp(mode, 'tardis')
            if strcmp(ID, '1126') % for 1126 choose nlreg without intermediate t2 step as no t2 was recorded
                fileName = [ID, '_run-',num2str(indRun),'_feat_detrended_highpassed_denoised_nlreg_2009c_3mm.nii'];
            else
                fileName = [ID, '_run-',num2str(indRun),'_feat_detrended_highpassed_denoised_t2nlreg_2009c_3mm.nii'];
            end
            fname = [pn.preprocFiles,  'D_preproc/',ID, '/preproc2/run-',num2str(indRun), '/' fileName];
        else
            fname = [a.session_info.run(indRun).data_path '/' a.session_info.run(indRun).data_files{:}];
        end
        
        if ~exist(fname) || isempty(a.session_info.run(indRun).data_files)
            warning(['File not available: ', ID, ' Run ', num2str(indRun)]);
            continue;
        end
        
        % If using preprocessing tools
        [img] = double(S_load_nii_2d(fname));
        
        img_backup = img;
        img = img_backup;
        % demean and detrend within run
        img = img-repmat(nanmean(img,2),1, size(img,2));
        img = detrend(img', 'linear')';
                
        % get average signal from WM and CSF masks

        WMmask = [pn.standards, 'tissuepriors/avg152T1_white_MNI_3mm.nii'];
        CSFmask = [pn.standards, 'tissuepriors/avg152T1_csf_MNI_3mm.nii'];
        [WMmask] = double(S_load_nii_2d(WMmask));
        WMsignal = img(WMmask==1,:); WMsignal = nanmean(WMsignal,1);
        [CSFmask] = double(S_load_nii_2d(CSFmask));
        CSFsignal = img(CSFmask==1,:); CSFsignal = nanmean(CSFsignal,1);

        % restrict signal of interest to final_coords         
        img = img(final_coords,:);
        
        %% regress out motion parameters
        
        % detrended regression estimates (Powers et al., 2014)
        % mean of voxels within CSF and WM masks, also demeaned
        
        % get FSL motion parameters (6 DOF)
        motionParamFile = [pn.PLSfilesIn, 'A_motionParams/',ID,'/',ID,'_sess-',num2str(indRun),'_motion_6dof.txt'];
        motionParams = load(motionParamFile);
        
        % get DVARS outliers
        load([pn.coordpath, 'S2_DVARS.mat'], 'DVARSout');
        DVARS_idx = find(strcmp(DVARSout.IDs, ID));
        Volumes2Censor = zeros(size(motionParams,1),1);
        Volumes2Censor(DVARSout.SignificantDVARS{DVARS_idx,indRun,1}) = 1;
        
        % demean motion parameters
        motionParams = motionParams-repmat(mean(motionParams,1),size(motionParams,1),1);
                
        % basically, build a linear model of regressor influence only using
        % good volumes, then apply this model also to bad volumes

        % add signal within 95% tissue probability CSF, WM masks
        
        % define regressor matrix
        X = [motionParams, WMsignal', CSFsignal'];
        % remove data and regressors that need to be scrubbed
        % (i) bad timepoints were censored from the regressors and BOLD data
        X_good = X; X_good(Volumes2Censor==1,:) = [];
        img_good = img; img_good(:,Volumes2Censor==1) = [];
        % (ii) the remaining good regressors were standardized (zero-mean, unit variance) and detrended
        X_good = zscore(X_good,[],1);
        X_good = detrend(X_good, 'linear');
        % (iii) a least-squares fit of good regressors to the good data generated beta values
        b_good=X_good\img_good'; % matrix implementation of multiple regression        
        % (iv) regressors from all timepoints (good and bad) underwent the same transformation that defined the good standardized regressors
        X_all = X;
        X_all = zscore(X_all,[],1);
        X_all = detrend(X_all, 'linear');
        % (v) the good betas were applied to regressors from all timepoints to generate modeled signal values at all timepoints
        AllPredict = X_all*b_good;
        % (vi) residuals were calculated for all timepoints as observed minus modeled BOLD values
        res = img-AllPredict';
        
%         figure; imagesc(zscore(res,[],2),[-1 1])
%         figure; imagesc(zscore(img,[],2),[-1 1])
        
        %% implement DVARS censoring (a la Powers et al., 2014)

        % performed via spectral interpolation (Powers et al., 2014; Parkes et al., 2018)
                
        censor.data = res';
        censor.t = ([1:size(img,2)]') * .645;
        censor.scrubmask = Volumes2Censor';
        censor.ofac = 4;
        censor.hifac = 1;
        
        voxbinsize = 100; % this chunking intends to avoid RAM problems
        voxbin = 1:voxbinsize:size(censor.data,2);
        voxbin = [voxbin size(censor.data,2)];

        data_surrogate = zeros(size(censor.data,1),size(censor.data,2));

        for v = 1:numel(voxbin)-1 % this takes huge RAM if all voxels
            fprintf(1, 'Processing voxel bin %u/%u\n', v,numel(voxbin)-1);
            data_surrogate(:,voxbin(v):voxbin(v+1)) = JP14_getTransform(censor.data(:,voxbin(v):voxbin(v+1)),censor.t,censor.scrubmask,censor.ofac,censor.hifac);
        end

        % insert surrogate data into real data at censored time points
        data_clean = censor.data;
        data_clean(censor.scrubmask==1,:) = data_surrogate(censor.scrubmask==1,:);
        data_clean = data_clean';
        
        %% save cleaned data
        
        save([pn.cleanDataOut, ID, '_run-',num2str(indRun), '_postProcessed.mat'], 'data_clean')        
                
        %% implement blind HRF deconvolution?
        
        %% plot exemplary influence of DVARS scrubbing (i.e. spectral interpolation)
%         scrm = censor.scrubmask;
%         scrm(scrm==0) = NaN;
% 
%         voxelToPlot = 9901;
%         h = figure('units','normalized','position',[.1 .1 .7 .5]);
%         subplot(2,1,1); hold on; 
%             l1 = plot(censor.data(:,voxelToPlot)', 'b', 'LineWidth', 2);
%             l2 = plot(data_surrogate(:,voxelToPlot)', 'r', 'LineWidth', 2);
%             l3 = plot(data_clean(voxelToPlot,:)', 'k', 'LineWidth', 2);
%             legend([l1, l2, l3], {'Original'; 'Spectral Reconstruction'; 'Cleaned signal'}); legend('boxoff');
%             xlim([1 1054])
%             title('Exemplary time-series censoring by spectral interpolation (Powers et al., 2014)')
%             ylabel('Amplitude')
%             xlabel('Time (TRs)');
%         subplot(2,1,2); hold on; 
%             plot(censor.data(:,voxelToPlot)'-data_surrogate(:,voxelToPlot)', 'k', 'LineWidth', 2)
%             sc = scatter(([1:size(img,2)]'), scrm, 100, 'filled', 'FillColor', 'r')
%             legend([sc], 'DVARS outliers'); legend('boxoff');
%             ylabel('Original-interpolated amplitude')
%             xlabel('Time (TRs)');
%             set(findall(gcf,'-property','FontSize'),'FontSize',24)
%             xlim([1 1054])       
%         
%         %% DEBUG:
%         %%% DELETE THIS LINE AFTER DEBUGGING ####
%         data_clean = res;
%         %%%%

        %% extract relevant volumes

        % load timing information
        load([pn.timing, ID, '_Run',num2str(indRun),'_regressors.mat'], 'Regressors', 'RegressorInfo');
        
        %block_data = NaN(4,size(data_clean,1),17);
        % encode in datamat
        for indCond = 1:numel(conditions)
            onsets = find(sum(Regressors(:,7:10),2)==indCond); % get onsets
            % encode data for each block            
            for indBlock = 1:numel(onsets)
            % TO DO: concatenate across runs!
                blockCount(indCond) = blockCount(indCond)+1;
                block_data_tmp(indCond,blockCount(indCond),:,:) = data_clean(:,onsets(indBlock):onsets(indBlock)+16); % encode 16 additional volumes
            end
        end

    end

    clear img;
    
%% save exemplary trial .nii files with regressed time series (across runs)

%     block_data_tmp_Full = NaN(1,1,64*76*64,17);
%     block_data_tmp_Full(:,:,final_coords,:) = block_data_tmp(1,1,:,1:17);
%     block_data_tmp_Full = reshape(block_data_tmp_Full, 64, 76, 64, 17);
% 
%     [NiiTemplate] = load_nii(fname); % load Nifty template
%     NiiTemplate.img = block_data_tmp_Full; % insert new cleaned data (64 x 76 x 64 x time x condition x repetition)
%     % change Nifty dimensions in header
%     NiiTemplate.hdr.dime.dim(5) = 17; 
%     save_nii(NiiTemplate, '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/B6_PLS_eventRelated/B_data/test.nii');

%% save (non-baselined) output for PLS

    for indCond = 1:numel(conditions)
        % normalize or average across all blocks
        block_data(indCond,:,:) = squeeze(nanmean(block_data_tmp(indCond,:,:,:),2));
        % use alternative normalizations here?
        curCondData = squeeze(block_data(indCond,:,:)); % 61003x17
        % reshape into PLS format
        a.st_datamat(indCond,:) = reshape(curCondData',[],1); %4*1220060
    end

    save([pn.PLSfilesOut, a.session_info.datamat_prefix '.mat'],'-struct','a','-mat');
    disp ([ID ' done!'])
    
%% save baselined output for PLS: 3-5 s following the stim onset (to capture fixation phase following HRF convolution)
% apply baselining on single trials vs. mean series?

    for indCond = 1:numel(conditions)
        % single-trial baseline: 2-4 s
        TRtime = .645.*([1:size(block_data_tmp,4)]-1); % minus 1 here, as the first TR captures the cue onset
        idxTR = find(TRtime > 3 & TRtime < 5); % use TRs between 3 and 5 seconds as baseline
        block_data_stbl = block_data_tmp-repmat(nanmean(block_data_tmp(:,:,:,idxTR),4),1,1,1,17);
        % normalize or average across all blocks
        block_data(indCond,:,:) = squeeze(nanmean(block_data_stbl(indCond,:,:,:),2));
        % use alternative normalizations here?
        curCondData = squeeze(block_data(indCond,:,:)); % 61003x17
        % reshape into PLS format
        a.st_datamat(indCond,:) = reshape(curCondData',[],1); %4*1220060
    end

    save([pn.PLSfilesOut, a.session_info.datamat_prefix '_STbl.mat'],'-struct','a','-mat');
    disp ([ID ' done!'])
    